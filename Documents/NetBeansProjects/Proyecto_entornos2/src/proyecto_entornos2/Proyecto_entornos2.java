
package proyecto_entornos2;

/*
 * @author AnaMF
 */
public class Proyecto_entornos2 {

    public static void main(String[] args) {
        operaciones_padre op = new operaciones_padre(1,2);
        suma_hijo s = new suma_hijo(2,3);
        multi_hijo m = new multi_hijo(5,6);
        divi_hijo d = new divi_hijo(24,12);
        resta_hijo r = new resta_hijo(30,15);
        
        op.operacion();
        s.operacion();
        m.operacion();
        d.operacion();
        r.operacion();
        
        operaciones_padre op1 = new suma_hijo(6,7);
        operaciones_padre op2 = new multi_hijo(2,2);
        operaciones_padre op3 = new divi_hijo(20,2);
        operaciones_padre op4 = new resta_hijo(64,24);
        
        op1.operacion();
        op2.operacion();
        op3.operacion();
        op4.operacion();
    }
    
}
